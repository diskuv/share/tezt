# HOWTO Release Tezt

- [ ] Push a commit that updates the version number in `lib_core/version.ml`.
- [ ] Tag with `git tag -a` and push this tag.
- [ ] Push a commit that adds the `dev` label to `lib_core/version.ml`.
- [ ] Make a pull request on https://github.com/ocaml/opam-repository/ to release on opam.
