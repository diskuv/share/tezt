(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(*****************************************************************************)

{
  open TSL_parser

  exception Error of string

  let error x = Printf.ksprintf (fun e -> raise (Error e)) x

  let bare_word = function
    | "true" -> TRUE
    | "false" -> FALSE
    | "not" -> NOT
    | string -> STRING string
}

(* The language is designed with the following in mind.
   - "tag" and "/tag" should parse as expected for backward-compatibility
     with the old command-line syntax. In particular, tags should not need quotes.
   - Tags that were valid before the introduction of TSL should ideally still
     be valid. With the exception of "true" and "false", this is the case.
   - It should not use single quotes, to make it easy to quote full expressions
     on the command-line.
   - Other than that, it should be as close as possible to OCaml's syntax,
     including symbols from Base such as "=~". *)

let bare_word_char_except_slash = ['a'-'z' 'A'-'Z' '0'-'9' '_' '-' '.']
let bare_word = bare_word_char_except_slash (bare_word_char_except_slash | '/')*

rule token = parse
  | ' '+ { token lexbuf }
  | '/' (bare_word as x) { SLASH_STRING x }
  | bare_word as x { bare_word x }
  | '"' { STRING (string (Buffer.create 128) lexbuf) }
  | '=' { EQUAL }
  | "<>" { NOT_EQUAL }
  | "=~" { MATCHES }
  | "=~!" { NOT_MATCHES }
  | '(' { LPAR }
  | ')' { RPAR }
  | "||" { OR }
  | "&&" { AND }
  | _ as c { error "parse error near: %C" c }
  | eof { EOF }

and string buffer = parse
  | '"' { Buffer.contents buffer }
  | "\\\"" { Buffer.add_char buffer '"'; string buffer lexbuf }
  | "\\\\" { Buffer.add_char buffer '\\'; string buffer lexbuf }
  | [^'"' '\\']+ as x { Buffer.add_string buffer x; string buffer lexbuf }
  | eof { error "unterminated string" }
