(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(*****************************************************************************)

(** Test Selection Language. *)

(** Parse a TSL expression. *)
val parse : string -> TSL_AST.t option

(** Convert a TSL expression to a string. *)
val show : ?always_parenthesize:bool -> TSL_AST.t -> string

(** Environment in which to evaluate TSL expressions. *)
type env = {file : string; title : string; tags : string list}

(** Evaluate a TSL expression. *)
val eval : env -> TSL_AST.t -> bool

(** Make a conjunction from a list. *)
val conjunction : TSL_AST.t list -> TSL_AST.t

(** Test whether a string is a valid tag.

    Tags:
    - must have a length between 1 and 32;
    - must only contain characters lowercase letters [a-z], digits [0-9] or underscores [_];
    - cannot be ["true"] nor ["false"]. *)
val is_valid_tag : string -> bool

(** Get the list of tags that appear in a TSL expression. *)
val tags : TSL_AST.t -> string list
