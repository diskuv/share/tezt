(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(*****************************************************************************)

(** Abstract Syntax Tree of the Test Selection Language. *)

open Base

(** Test properties that can be queried using string operators. *)
type string_var = File | Title

(** Comparison operators for strings. *)
type string_operator = Is of string | Matches of rex

(** AST of TSL. *)
type t =
  | True
  | False
  | String_predicate of string_var * string_operator
  | Has_tag of string
  | Not of t
  | And of t * t
  | Or of t * t
