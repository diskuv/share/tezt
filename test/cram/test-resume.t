To be able to resume a run, one first needs to create a resume file.

  $ ./tezt.sh --file main.ml resume --resume
  [SUCCESS] (1/3) Success
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (2/3, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [1]

If one then resumes from this file, tests that were already successful are not run again.

  $ ./tezt.sh --file main.ml resume --resume
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (2/3, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [1]

This also works with --keep-going.

  $ ./tezt.sh --file main.ml resume --resume --keep-going
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (2/3, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [SUCCESS] (3/3, 1 failed) Success 2
  [1]
  $ ./tezt.sh --file main.ml resume --resume --keep-going
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (2/3, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [1]

To restart, delete the resume file.

  $ rm ./tezt-resume.json
  $ ./tezt.sh --file main.ml resume --resume
  [SUCCESS] (1/3) Success
  Starting test: Failing test
  [error] Always failing test
  [FAILURE] (2/3, 1 failed) Failing test
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Failing test'
  [1]
  $ rm ./tezt-resume.json
