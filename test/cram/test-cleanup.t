Test Test.declare_clean_up_function:

  $ ./tezt.sh --file main.ml cleanup -v -k
  Starting test: Clean up hook: nothing
  [SUCCESS] (1/4) Clean up hook: nothing
  Starting test: Clean up hook: log
  Clean up hook was triggered for test "Clean up hook: log" from test/cram/main.ml (test_result = Successful).
  [SUCCESS] (2/4) Clean up hook: log
  Starting test: Clean up hook: fail and log
  [error] failing on purpose
  Clean up hook was triggered for test "Clean up hook: fail and log" from test/cram/main.ml (test_result = Failed).
  [FAILURE] (3/4, 1 failed) Clean up hook: fail and log
  Try again with: _build/default/main.exe --verbose --file test/cram/main.ml --title 'Clean up hook: fail and log'
  Starting test: Clean up hook: raise
  [warn] Function registered using Test.declare_clean_up_function raised: Clean up hook was triggered.
  [SUCCESS] (4/4, 1 failed) Clean up hook: raise
  [1]
