One can list all tests.

  $ ./tezt.sh --list
  +-------------------+-----------------------------+-----------------------------+
  |       FILE        |            TITLE            |            TAGS             |
  +-------------------+-----------------------------+-----------------------------+
  | test/cram/main.ml | Success                     | retry, success, resume      |
  | test/cram/main.ml | Fail every other run test   | retry, fail, flake          |
  | test/cram/main.ml | Failing test                | retry, fail, always, resume |
  | test/cram/main.ml | Success 2                   | success, resume             |
  | a/b/c.ml          | a/b/c.ml                    | selection                   |
  | a/b/g.ml          | a/b/g.ml                    | selection                   |
  | a/c.ml            | a/c.ml                      | selection                   |
  | d.ml              | d.ml                        | selection                   |
  | e.ml              | e.ml                        | selection                   |
  | test/cram/main.ml | Cli.get                     | cli, options                |
  | test/cram/main.ml | 4s test                     | job_selection               |
  | test/cram/main.ml | 2s test (1)                 | job_selection               |
  | test/cram/main.ml | 2s test (2)                 | job_selection               |
  | test/cram/main.ml | Clean up hook: nothing      | cleanup                     |
  | test/cram/main.ml | Clean up hook: log          | cleanup                     |
  | test/cram/main.ml | Clean up hook: fail and log | cleanup                     |
  | test/cram/main.ml | Clean up hook: raise        | cleanup                     |
  | test/cram/main.ml | clap                        | clap                        |
  | test/cram/main.ml | log at exit                 | at_exit                     |
  +-------------------+-----------------------------+-----------------------------+
  $ ./tezt.sh --list-tsv
  test/cram/main.ml	Success	retry success resume
  test/cram/main.ml	Fail every other run test	retry fail flake
  test/cram/main.ml	Failing test	retry fail always resume
  test/cram/main.ml	Success 2	success resume
  a/b/c.ml	a/b/c.ml	selection
  a/b/g.ml	a/b/g.ml	selection
  a/c.ml	a/c.ml	selection
  d.ml	d.ml	selection
  e.ml	e.ml	selection
  test/cram/main.ml	Cli.get	cli options
  test/cram/main.ml	4s test	job_selection
  test/cram/main.ml	2s test (1)	job_selection
  test/cram/main.ml	2s test (2)	job_selection
  test/cram/main.ml	Clean up hook: nothing	cleanup
  test/cram/main.ml	Clean up hook: log	cleanup
  test/cram/main.ml	Clean up hook: fail and log	cleanup
  test/cram/main.ml	Clean up hook: raise	cleanup
  test/cram/main.ml	clap	clap
  test/cram/main.ml	log at exit	at_exit

While listing tests, one can filter tests.

  $ ./tezt.sh --list --file main.ml
  +-------------------+-----------------------------+-----------------------------+
  |       FILE        |            TITLE            |            TAGS             |
  +-------------------+-----------------------------+-----------------------------+
  | test/cram/main.ml | Success                     | retry, success, resume      |
  | test/cram/main.ml | Fail every other run test   | retry, fail, flake          |
  | test/cram/main.ml | Failing test                | retry, fail, always, resume |
  | test/cram/main.ml | Success 2                   | success, resume             |
  | test/cram/main.ml | Cli.get                     | cli, options                |
  | test/cram/main.ml | 4s test                     | job_selection               |
  | test/cram/main.ml | 2s test (1)                 | job_selection               |
  | test/cram/main.ml | 2s test (2)                 | job_selection               |
  | test/cram/main.ml | Clean up hook: nothing      | cleanup                     |
  | test/cram/main.ml | Clean up hook: log          | cleanup                     |
  | test/cram/main.ml | Clean up hook: fail and log | cleanup                     |
  | test/cram/main.ml | Clean up hook: raise        | cleanup                     |
  | test/cram/main.ml | clap                        | clap                        |
  | test/cram/main.ml | log at exit                 | at_exit                     |
  +-------------------+-----------------------------+-----------------------------+
  $ ./tezt.sh --list-tsv resume
  test/cram/main.ml	Success	retry success resume
  test/cram/main.ml	Failing test	retry fail always resume
  test/cram/main.ml	Success 2	success resume
  $ ./tezt.sh --list-tsv resume --skip 1
  test/cram/main.ml	Failing test	retry fail always resume
  test/cram/main.ml	Success 2	success resume
  $ ./tezt.sh --list-tsv resume --only 1
  test/cram/main.ml	Success	retry success resume
