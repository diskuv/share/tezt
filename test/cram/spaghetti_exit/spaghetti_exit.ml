(* The goal is to log from an at_exit that is registered *before* the one in the Log module,
   so that this at_exit executes *after* the one in the Log module
   (at_exit callbacks are called in reverse order).

   For that, we need a library that is linked *before* Tezt,
   so it cannot access the Log module.
   But one can use a reference to modify the callback and have it use the Log module. *)

let callback = ref (fun () -> ())

let () = at_exit (fun () -> !callback ())
