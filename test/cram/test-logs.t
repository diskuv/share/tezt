Default logs only show errors, but one can show more.

  $ ./tezt.sh --file main.ml --title Success --info
  Starting test: Success
  Success test.
  [SUCCESS] (1/1) Success
  $ ./tezt.sh --file main.ml --title Success --verbose
  Starting test: Success
  Success test.
  This is a verbose log.
  [SUCCESS] (1/1) Success

Logging at exit works for standard output.

  $ ./tezt.sh --file main.ml --log-file tmp-log-file-of-cram-test.log at_exit --info
  Starting test: log at exit
  [SUCCESS] (1/1) log at exit
  Normally-registered at_exit.
  [warn] Cannot log "Spaghetti-registered at_exit." to file: file is closed. Did you try to log from an at_exit handler that is registered before the Log module? None of the following log messages will appear in the log file.
  Spaghetti-registered at_exit.
  Another spaghetti-registered at_exit.

In log files though, we only see logs from at_exit callbacks if those at_exit were
registered *after* the at_exit in the Log module.

  $ cut -b 16- tmp-log-file-of-cram-test.log
  Starting test: log at exit
  [SUCCESS] (1/1) log at exit
  Normally-registered at_exit.
