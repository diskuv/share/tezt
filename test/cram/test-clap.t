One can define and pass custom arguments.

  $ ./tezt.sh --test clap -i
  Starting test: clap
  Custom arg is default.
  [SUCCESS] (1/1) clap
  $ ./tezt.sh --test clap --custom-arg hello -i
  Starting test: clap
  Custom arg is hello.
  [SUCCESS] (1/1) clap

It even appears in --help.

  $ ./tezt.sh --help | grep -A 1 'custom-arg VALUE'
      --custom-arg VALUE (default: default)
          A custom argument for a specific test.
